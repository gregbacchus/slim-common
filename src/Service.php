<?php
namespace Common;

abstract class Service extends \Slim\App
{
    abstract public function mountApis();

    protected function mount(string $pattern, \Common\Api $api)
    {
        $this->group($pattern, function () use ($api) {
            $api->addRoutes($this);
        });
    }
}
